import React, { Component } from 'react'; //different
//import logo from './logo.svg';
import Main from './components/MainComponent';
import './App.css';
import { Provider } from 'react-redux';
import { ConfigureStore } from './redux/configureStore';
import { BrowserRouter } from 'react-router-dom';

const store = ConfigureStore();

class App extends Component { //different
  render() {
    return(
      <Provider store={store}>
        <BrowserRouter>
          <div>
            <Main/>
          </div>  
        </BrowserRouter>  
      </Provider>        
    );
  };
}

export default App;
